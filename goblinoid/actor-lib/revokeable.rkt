#lang racket/base

(provide spawn-revokeable)

(require "../core.rkt")

(define (spawn-revokeable target)
  (define revoked?
    (spawn-cell #f))
  (define forwarder
    (spawn
     (make-keyword-procedure
      (lambda (kws kw-args become . args)
        (when (revoked?)
          (error "Access revoked!"))
        (keyword-apply call kws kw-args target args)))))
  (define revoker
    (spawn
     (lambda (bcom)
       (revoked? #t))))
  (list forwarder revoker))

(module+ test
  (require rackunit
           racket/match
           racket/contract)
  (define am (make-actormap))
  (define royal-admission
    (actormap-spawn!
     am (lambda (bcom)
          "The Queen will see you now.")))
  (match-define (list royal-forwarder royal-revoker)
    (actormap-run! am (lambda ()
                        (spawn-revokeable royal-admission))))

  (check-equal?
   (actormap-peek am royal-forwarder)
   "The Queen will see you now.")

  (actormap-poke! am royal-revoker)

  (check-exn
   any/c
   (lambda () (royal-forwarder))))
